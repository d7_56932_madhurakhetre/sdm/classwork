const db = require('./db');
const utils = require("./utils");
const cors = require("cors");
const express = require("express");

const app = express();

app.use(express.json());

app.use(cors("*"));

//create apis
app.get("/topics",(req,resp)=>{
    const sql='select * from topics';
    db.execute(sql,(err,data)=>{
        resp.send(utils.createresult(err,data));
    })
})

app.post("/add",(req,resp)=>{
    const {name}=req.body
    const sql=`insert into topics (name) values('${name}')`;
    db.execute(sql,(err,data)=>{
        resp.send(utils.createresult(err,data));
    })
})


app.listen(4001,'0.0.0.0',()=>{
    console.log("server started on port 4001");
})